# ARIA Mailer

This is an ARIA wrapper around both the legacy PHPMailer, and Swiftmailer.

## Installation

Add the repository to your composer.json:

```
"repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/aria-php/aria-mailer.git"
    }
  ],
```

and then `composer require aria-php/aria-mailer` from within your project.

## Usage

Creating a PHP Mailer from ARIA

```
$mail = new ariamailer(new phpmailer($this->settings)); 
```

Creating a Swiftmailer wrapper around mailgun

```
use Mailgun\Mailgun;
use cspoo\Swiftmailer\MailgunBundle\Service\MailgunTransport;


...


// Create a mailgun object
$mailgun = Mailgun::create($apikey, 'https://api.en.mailgun.com');
$transporter = new MailgunTransport(
    $mailgun, // Mailgun object
    $config['mg_domain'], // Your domain
    $logger // PSR Compatible logging interface
); 
$mail = new ariamailer(new swiftmailer($transporter));
  	 
```

Sending an email:


```
$mail->setFrom('jim@example.com', 'Jim Kirk');

$mail->setTo('spock@example.com', 'Mr Spock');

$mail->setSubject("Attn: Spock!");
$mail->setBody("Analysis Mr Spock!");
$mail->setHTMLBody("<b>Analysis Mr Spock!</b>");

// Now send
$mail->send();
```

