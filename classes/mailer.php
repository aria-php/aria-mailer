<?php

namespace ARIA\mailer;
use ARIA\mailer\delivery\method;

/**
 * Abstract mailer.
 * This defines a mailer, aimed to be a drop in replacement for PHP Mailer used by ARIA.
 */
class mailer 
{
    private $method;
    
    /**
     * Construct a mailer with a given delivery method.
     * @param method $method
     */
    public function __construct(method $method) 
    {
        $this->method = $method;
    }
    
    public function setFrom( string $address, string $name = '', bool $auto = true) 
    {
      return $this->method->setFrom( $address, $name ); 
    }
    
    public function addTo( string $address, string $name = '' ) 
    {
      return $this->method->addTo($address, $name);
    }
    
    public function addCC( string $address, string $name = '' ) 
    {
      return $this->method->addCC($address, $name);
    }
    
    public function addBCC( string $address, string $name = '' ) 
    {
      return $this->method->addBCC($address, $name);
    }
    
    public function addReplyTo( string $address, string $name = '' ) 
    {
      return $this->method->addReplyTo($address, $name);
    }
  
    public function addHeader( string $header, string $value )
    {
      return $this->method->addHeader($header, $value);
    }
  
    public function addAttachment( string $path, string $name, string $mime ) 
    {
      return $this->method->addAttachment($path, $name, $mime);
    }

    public function setSubject(string $subject) {
      return $this->method->setSubject($subject);
    }
    
    public function setBody(string $body) {
      return $this->method->setBody($body);
    }
    
    public function setHTMLBody(string $body ) {
      return $this->method->setHTMLBody($body);
    }
    
    public function send() {
      return $this->method->send();
    }

    /**
     * Validate an email address.
     * @param string $address
     * @return bool
     */
    public static function validateAddress(string $address) : bool 
    {
        
        return filter_var($address, FILTER_VALIDATE_EMAIL) !== false;
        
    }
}
