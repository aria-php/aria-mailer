<?php

namespace ARIA\mailer\delivery;

/**
 * Delivery method interface.
 * Defines a method for interfacing with a delivery method subsystem.
 */
abstract class method {

  protected $settings = [];

  public function __construct(array $settings = []) 
  {
    $this->settings = $settings;
  }

  public function setConfig(string $setting, $value) 
  {
    $this->settings[$setting] = $value;
  }

  public function getConfig(string $setting) 
  {
    return $this->settings[$setting];
  }

  abstract public function setFrom( string $address, string $name = '' ) : bool;
  
  abstract public function addTo( string $address, string $name = '' ) : bool;
  
  abstract public function addCC( string $address, string $name = '' ) : bool;
  
  abstract public function addBCC( string $address, string $name = '' ) : bool;
  
  abstract public function addReplyTo( string $address, string $name = '' ) : bool;
  
  abstract public function addHeader( string $header, string $value ) : bool;
  
  abstract public function addAttachment( string $path, string $name, string $mime ) : bool;
  
  abstract public function setSubject( string $subject ) : bool;
  
  abstract public function setBody( string $body ) : bool;
  
  abstract public function setHTMLBody( string $html ) : bool;
  
  abstract public function send() : bool; 
  
}
