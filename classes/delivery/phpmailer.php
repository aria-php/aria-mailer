<?php

namespace ARIA\mailer\delivery;

use PHPMailer\PHPMailer\PHPMailer as PHPMailerActual;
use PHPMailer\PHPMailer\Exception as PHPMailerException;

class phpmailer extends method {

  private $mailer;

  public function __construct(array $settings = array()) {
    parent::__construct($settings);

    $this->mailer = new PHPMailerActual(true);
    $this->mailer->CharSet = 'utf-8';

    // Setup SMTP
    if ($this->settings['smtp']) {
      
      $this->mailer->isSMTP();
      
      //$this->mailer->SMTPDebug  = 2;
      
      $this->mailer->Host = $this->settings['smtp'];
      $this->mailer->Port = $this->settings['smtpport'];
      $this->mailer->SMTPSecure = ($this->settings['smtpssl'] ? 'tls' : false);
      $this->mailer->SMTPAuth = ($this->settings['smtpuser'] ? true : false);
      
      if ($this->mailer->SMTPAuth) {
        $this->mailer->Username = $this->settings['smtpuser'];
        $this->mailer->Password = $this->settings['smtppass'];
      }
    }
  }

  public function addAttachment(string $path, string $name, string $mime): bool 
  {
    return $this->mailer->addAttachment($path, $name, PHPMailer::ENCODING_BASE64, $mime);
  }

  public function addBCC(string $address, string $name = ''): bool 
  {
    return $this->mailer->addBCC($address, $name);
  }

  public function addCC(string $address, string $name = ''): bool 
  {
    return $this->mailer->addCC($address, $name);
  }

  public function addHeader(string $header, string $value): bool 
  {
    return $this->mailer->addCustomHeader($header, $value);
  }

  public function addReplyTo(string $address, string $name = ''): bool 
  {
    return $this->mailer->addReplyTo($address, $name);
  }

  public function addTo(string $address, string $name = ''): bool 
  {
    return $this->mailer->addAddress($address, $name);
  }

  public function setFrom(string $address, string $name = ''): bool 
  {
    return $this->mailer->setFrom($address, $name);
  }

  public function setBody(string $body): bool {
    $this->mailer->Body = $body;
    
    return true;
  }

  public function setHTMLBody(string $html): bool {
    $this->mailer->isHTML(true);
    $this->mailer->Body = $html;
    $this->mailer->AltBody = $this->mailer->html2text($html, function($html) { 
      return (new \Html2Text\Html2Text($html))->getText();
    });
    
    return true;
  }

  public function setSubject(string $subject): bool {
    $this->mailer->Subject = $subject;
    
    return true;
  }

  public function send(): bool {
    return $this->mailer->send();
  }

}
