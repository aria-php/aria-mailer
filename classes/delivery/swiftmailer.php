<?php

namespace ARIA\mailer\delivery;

class swiftmailer extends method {
  
  private $transport;
  
  private $message;
  
  public function __construct(\Swift_Transport $transporter) {
    $this->transport = $transporter;
    
    $this->message = new \Swift_Message();
  }
  
  public function addAttachment(string $path, string $name, string $mime): bool {
    
    $attachment = new \Swift_Attachment(new \Swift_ByteStream_FileByteStream($path), $name, $mime); // Lets hope this is smart enough to handle this without loading it direcly into memory!
    $this->message->attach($attachment);
   
    return true;
  }

  public function addBCC(string $address, string $name = ''): bool {
    $this->message->addBcc($address, empty($name)?null:$name);
    
    return true;
  }

  public function addCC(string $address, string $name = ''): bool {
    $this->message->addCc($address, empty($name)?null:$name);
    
    return true;
  }

  public function addHeader(string $header, string $value): bool {
    $headers = $this->message->getHeaders();
    $headers->addTextHeader($header, $value);
    
    return true;
  }

  public function addReplyTo(string $address, string $name = ''): bool {
    $this->message->addReplyTo($address, empty($name)?null:$name);
    
    return true;
  }

  public function addTo(string $address, string $name = ''): bool {
    $this->message->addTo($address, empty($name)?null:$name);
    
    return true;
  }

  public function send(): bool 
  {
    $mailer = new \Swift_Mailer($this->transport);
    
    $sent = $mailer->send($this->message, $failedRecipients);
    
    if (!empty($failedRecipients) || $sent == 0) 
      return false;
    
    return true;
  }

  public function setBody(string $body): bool {
    $this->message->addPart($body, 'text/plain');
    
    return true;
  }

  public function setFrom(string $address, string $name = ''): bool {
    $this->message->setFrom($address, empty($name)?null:$name);
    
    return true;
  }

  public function setHTMLBody(string $html): bool {
    $this->message->setBody($html, 'text/html');
    
    return true;
  }

  public function setSubject(string $subject): bool {
    $this->message->setSubject($subject);
    
    return true;
  }

}